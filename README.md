# SSVEP-CNN-Demo
SSVEP-CNN-Demo - Demonstration of SKINTRONICS Universal 2-channel SSVEP Classification from 5 classes.

## System Requirements
This program is written in Python 3, and requires a python environment to run.

## Installation Guide
Use the following command to install or upgrade the following Python packages, using the requirements.txt found in the project's root folder:

pip install -U -r requirements.txt

## Sample Data
Sample data can be downloaded from the following repository hosted by Open Science Framework: https://osf.io/nyfa7/

The files should be extracted directly into the root directory of the python project. The data is stored in .mat files (MATLAB format), and is separated into train (w128_train/) and test (w128_test) folders, with files representing trials from each subject. Files can be easily copy-pasted to and from each folder to test different combinations.

## Instructions to Run
There are two versions of the code to run.
If you have tensorflow-gpu set up, run demo_gpu.py, otherwise use demo_nogpu.py.

## Expected Output
Test score: 0.2699780395036938 , Test accuracy: 0.9192592592592592
Elapsed Time (ms):  20941
