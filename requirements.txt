tensorflow==1.12.0
keras==2.2.4
pandas==0.23.0
scipy==1.1.0
sklearn==0.19.1
numpy==1.14.3
