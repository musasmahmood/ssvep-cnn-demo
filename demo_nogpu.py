import glob
import os
import time

os.environ["CUDA_VISIBLE_DEVICES"] = ""

import numpy as np
import pandas as pd
import tensorflow as tf
from keras.layers import Dense, Dropout, Flatten, Conv1D
from keras.models import Sequential
from keras.models import load_model
from scipy.io import loadmat


def current_time_ms():
    return int(round(time.time() * 1000))


def prep_dir(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    return directory


def load_data(directory, image_shape, m_key_x, m_key_y):
    x_train_data = np.empty([0, *image_shape], np.float32)
    y_train_data = np.empty([0], np.float32)
    training_files = glob.glob(directory + "/*.mat")
    for f in training_files:
        x_array = loadmat(f).get(m_key_x)
        y_array = loadmat(f).get(m_key_y)
        y_array = y_array.reshape([np.amax(y_array.shape)])
        x_train_data = np.concatenate((x_train_data, x_array), axis=0)
        y_train_data = np.concatenate((y_train_data, y_array), axis=0)
    y_train_data = np.asarray(pd.get_dummies(y_train_data).values).astype(np.float32)

    print("Loaded Data Shape: X:", x_train_data.shape, " Y: ", y_train_data.shape)

    return x_train_data, y_train_data


# Options for Window Length
win_len = 128

DATASET = 'ssvep'

description = DATASET + '_' + str(win_len) + '_classify'
keras_model_name = description + '.h5'
model_dir = prep_dir('model_exports/')
keras_file_location = model_dir + keras_model_name
# Start Timer:
start_time_ms = current_time_ms()

# Setup:
TRAIN = True
TEST = True

data_directory_train = 'w' + str(win_len) + '_train'
data_directory_test = 'w' + str(win_len) + '_test'

key_x = 'X'
key_y = 'Y'

batch_size = 256
epochs = 60
output_folder = 'data_out/' + description + '/'

# To load the data:
input_shape = [2, win_len]

x_train, y_train = load_data(data_directory_train, input_shape, m_key_x=key_x, m_key_y=key_y)
x_test, y_test = load_data(data_directory_test, input_shape, key_x, key_y)

# Tranposing for conv
x_train = np.transpose(x_train, (0, 2, 1))
x_test = np.transpose(x_test, (0, 2, 1))
input_shape = [win_len, 2]
print(input_shape)

print("Processed Data Shape: X:", x_train.shape, " Y: ", y_train.shape)
print("Processed Test Shape: X:", x_test.shape, " Y: ", y_test.shape)


def build_model():
    model = Sequential()
    model.add(Conv1D(64, 8, strides=2, activation='relu', padding='same', input_shape=input_shape))
    model.add(Conv1D(128, 8, strides=2, activation='relu', padding='same'))
    model.add(Flatten())
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.25))
    model.add(Dense(output_dim=5, activation='softmax'))
    model.compile(loss="categorical_crossentropy", optimizer='adadelta', metrics=["accuracy"])
    return model


with tf.device('/cpu:0'):
    model = []
    if TRAIN:
        if os.path.isfile(keras_file_location):
            model = load_model(keras_file_location)
        else:
            model = build_model()
        print(model.summary())

        model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=1)
        model.save(keras_file_location)

    if os.path.isfile(keras_file_location):
        if not TRAIN:
            model = load_model(keras_file_location)
            print(model.summary())
            if TEST:
                score, acc = model.evaluate(x_test, y_test, batch_size=128, verbose=1)
                print('Test score: {} , Test accuracy: {}'.format(score, acc))
                y_prob = model.predict(x_test)
        else:
            if TEST and model is not None:
                score, acc = model.evaluate(x_test, y_test, batch_size=128, verbose=1)
                print('Test score: {} , Test accuracy: {}'.format(score, acc))
                y_prob = model.predict(x_test)
            else:
                print('This should never happen: model does not exist')
                exit(-1)
    else:
        print("Model Not Found!")
        if not TRAIN:
            exit(-1)

print('Elapsed Time (ms): ', current_time_ms() - start_time_ms)
print('Elapsed Time (min): ', (current_time_ms() - start_time_ms) / 60000)
